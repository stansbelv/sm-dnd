<h1 align="center">
  Drag and drop
</h1>

This project implements drag and drop component for the card component.

## 🚅 Quick start

1.  **Clone the repository.**

    ```shell
    # Clone the repo
    git clone https://gitlab.com/stansbelv/sm-dnd.git
    ```

1.  **Install the dependencies.**

    Navigate into app directory and install the necessary dependencies.

    ```shell
    # Navigate to the directory
    cd sm-dnd/

    # Install the dependencies
    yarn
    ```

1.  **Browse the stories**

    Run `yarn storybook` to see your component's stories at `http://localhost:6006`

1.  **Test**

    Run `yarn test --watchAll` to test component's stories.

1.  **Run the react app!**

    Run `yarn start` to see the app at `http://localhost:3000`

## 🔎 What's inside?

A quick look at the top-level files and directories included with this template.

    .
    ├── .storybook
    ├── node_modules
    ├── public
    ├── src
    ├── .env
    ├── .gitignore
    ├── LICENSE
    ├── package.json
    ├── yarn.lock
    └── README.md

1.  **`.storybook`**: This directory contains Storybook's [configuration](https://storybook.js.org/docs/react/configure/overview) files.

2.  **`node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages).

3.  **`public`**: This directory will contain the development and production build of the site.

4.  **`src`**: This directory will contain all of the code related to what you will see on your application.

5.  **`.env`**: Simple text configuration file for controlling the application's environment constants.

6.  **`.gitignore`**: This file tells git which files it should not track or maintain during the development process of your project.

7.  **`LICENSE`**: The template is licensed under the MIT licence.

8.  **`tsconfig.json`**: TS configuration file.

9.  **`package.json`**: Standard manifest file for Node.js projects, which typically includes project specific metadata (such as the project's name, the author among other information). It's based on this file that npm will know which packages are necessary to the project.

10. **`yarn.lock`**: This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(Do not change it manually).**

11. **`README.md`**: A text file containing useful reference information about the project.
