import { Meta, Story } from "@storybook/react";
import Card, { CardProps } from "./Card";

export default {
  component: Card,
  title: "Card",
  decorators: [
    (story) => (
      <div style={{ padding: "3rem", backgroundColor: "#f7f7f7" }}>
        {story()}
      </div>
    ),
  ],
  argTypes: {
    id: {
      description: "ID of the card.",
    },
    title: {
      description: "Title of the card.",
    },
    description: {
      description: "Description of the card.",
    },
    onClose: {
      description: "Returns the id of the card and calls passed in method.",
    },
  },
} as Meta;

const Template: Story<CardProps> = (args) => <Card {...args} />;

export const Default = Template.bind({});
Default.args = {
  id: 1,
  title: "Test",
  description: "Lore impsum memo",
};

export const WithOnClose = Template.bind({});
WithOnClose.args = {
  id: 2,
  title: "Test",
  description: "Lore impsum memo",
  onClose: (id: number) => {
    console.log(`Closed ${id}`);
  },
};
