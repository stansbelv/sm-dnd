import "./Card.css";

export interface CardProps {
  /**
   *  ID of the card.
   */
  id: number;
  /**
   *  Title of the card.
   */
  title: string;
  /**
   *  Description of the card.
   */
  description: string;
  /**
   *  Returns the id of the card and calls passed in method.
   */
  onClose?(id: number): void;
}

function Card({ id, title, description, onClose }: CardProps) {
  return (
    <div className="card">
      <div className="card-content">
        <div className="card-title">{title}</div>
        <div className="card-description">{description}</div>
      </div>

      {onClose && (
        <button className={"close-icon"} onClick={() => onClose(id)}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="12"
            height="12"
            viewBox="0 0 24 24"
          >
            <path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z" />
          </svg>
        </button>
      )}
    </div>
  );
}

export default Card;
