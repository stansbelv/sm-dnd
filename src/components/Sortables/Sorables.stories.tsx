import { Meta, Story } from "@storybook/react";
import { CardProps } from "../Card/Card";
import Sortables, { SortableItem, SortablesProps } from "./Sortables";

export default {
  component: Sortables,
  title: "Sortables",
  decorators: [
    (story) => (
      <div style={{ padding: "3rem", backgroundColor: "#f7f7f7" }}>
        {story()}
      </div>
    ),
  ],
  argTypes: {
    isDraggingDisabled: {
      description: "Disables dragging of the item.",
    },
    isDraggedOver: {
      description:
        "Indicates if the item has been dragged over by another one.",
    },
    isDragging: {
      description: "Indicates if the current item is being dragged.",
    },
    onDragStart: {
      description:
        "NOT_IMPLEMENTED: Needs more info from the team. Method that will be called on start of the drag.",
    },
    onDragEnd: {
      description:
        "NOT_IMPLEMENTED: Needs more info from the team. Method that will be called when the drag ends.",
    },
    onDrop: {
      description:
        "NOT_IMPLEMENTED: Needs more info from the team. Method that will be called when the item is dropped in the new place.",
    },
  },
} as Meta;

const Template: Story<SortablesProps> = (args) => <Sortables {...args} />;

export const Default = Template.bind({});
Default.args = {
  data: [
    {
      id: 1,
      title: "One",
      description: "Lore impsum move",
    },

    {
      id: 2,
      title: "Two",
      description: "Lore impsum move",
    },
    {
      id: 3,
      title: "Three",
      description: "Lore impsum move",
    },
    {
      id: 4,
      title: "Four",
      description: "Lore impsum move",
    },
    {
      id: 5,
      title: "Five",
      description: "Lore impsum move",
    },
  ],
};

export const WithDisabled = Template.bind({});
WithDisabled.args = {
  data: [
    {
      id: 1,
      title: "One ",
      description: "Lore impsum move",
      isDraggingDisabled: true,
    },

    {
      id: 2,
      title: "Two",
      description: "Lore impsum move",
    },
    {
      id: 3,
      title: "Three",
      description: "Lore impsum move",
      isDraggingDisabled: true,
    },
    {
      id: 4,
      title: "Four",
      description: "Lore impsum move",
    },
  ],
};

export const withOnClose = Template.bind({});
withOnClose.args = {
  data: [
    {
      id: 1,
      title: "One ",
      description: "Lore impsum move",
      onClose: (id: number) => {
        console.log(`Closed card #${id}`);
      },
    },
  ],
};
