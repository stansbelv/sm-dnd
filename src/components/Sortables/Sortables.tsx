import classNames from "classnames";
import React, { useState } from "react";
import Card, { CardProps } from "../Card/Card";
import "./Sortables.css";

export type SortableCard = CardProps & SortableItem;

// TODO: Discuss with team if should become generic
export interface SortablesProps {
  data: SortableCard[];
}

export interface SortableItem {
  /**
   *  Disables dragging of the item.
   */
  isDraggingDisabled?: boolean;
  /**
   *  Indicates if the item has been dragged over by another one.
   */
  isDraggedOver?: boolean;
  /**
   *  Indicates if the current item is being dragged.
   */
  isDragging?: boolean;
  /**
   *  NOT_IMPLEMENTED: Needs more info from the team. Method that will be called on start of the drag.
   */
  onDragStart?: () => void;
  /**
   *  NOT_IMPLEMENTED: Needs more info from the team. Method that will be called when the drag ends.
   */
  onDragEnd?: () => void;
  /**
   *  NOT_IMPLEMENTED: Needs more info from the team. Method that will be called when the item is dropped in the new place.
   */
  onDrop?: () => void;
}

//
// Helper function to swap array elements
//
function swapArrayElements<T>(arr: T[], a: number, b: number) {
  arr[a] = arr.splice(b, 1, arr[a])[0];
  return arr;
}

export default function Sortables({ data }: SortablesProps) {
  const [dragItem, setDragItem] = useState<number>(-1);
  const [dragOverItem, setOverDragItem] = useState<number>(-1);
  const [isDragging, setIsDragging] = useState<boolean>(false);

  //
  // Drag event controls
  //
  const handleDragStart = (index: number) => {
    setDragItem(index);
    setOverDragItem(index);
    setIsDragging(true);
  };

  const handleDragEnd = (index: number) => {
    setDragItem(-1);
    setOverDragItem(-1);
    setIsDragging(false);
  };

  // TODO: Discuss with team if those methods should be available
  const handleDragEnter = <T,>(e: React.DragEvent<T>, index: number) => {};
  const handleDragLeave = <T,>(e: React.DragEvent<T>, index: number) => {};

  //
  // Handle drop and re-order list
  //
  const handleDrop = <T,>(
    e: React.DragEvent<T>,
    index: number,
    disabled?: boolean,
  ) => {
    //Disabled items won't be reordered
    if (!disabled) {
      swapArrayElements(data, index, dragItem);
      setDragItem(-1);
      setOverDragItem(-1);
    }
  };

  const handleDragOver = <T,>(e: React.DragEvent<T>, index: number) => {
    e.preventDefault();
    setOverDragItem(index);
  };

  return (
    <table className="drag-and-drop">
      <tbody>
        {data.map((item, index) => (
          <tr
            className={classNames({
              draggable: true,
              disabled: item.isDraggingDisabled,
              //The dragged item class, does nothing but can be used (Ask team)
              isDragging: !item.isDraggingDisabled && index === dragItem,
              //Highligh dragged over item
              isDraggedOver:
                !item.isDraggingDisabled &&
                index === dragOverItem &&
                index !== dragItem,
              //Highligh all disabled cards when user is dragging item
              "disabled-highlight": item.isDraggingDisabled && isDragging,
            })}
            draggable={item.isDraggingDisabled ? false : true}
            key={index}
            onDragStart={() => handleDragStart(index)}
            onDragEnd={() => handleDragEnd(index)}
            onDragEnter={(e) => handleDragEnter(e, index)}
            onDragLeave={(e) => handleDragLeave(e, index)}
            onDrop={(e) => handleDrop(e, index, item.isDraggingDisabled)}
            onDragOver={(e) => handleDragOver(e, index)}
          >
            <td>
              <div className="drag-container">
                <span className="drag-icon">☰</span>
                <div className="drag-content">
                  <Card
                    id={item.id}
                    title={item.title}
                    description={item.description}
                    onClose={item.onClose}
                  />
                </div>
              </div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
