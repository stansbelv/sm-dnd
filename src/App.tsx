import "./App.css";
import Sortables, { SortableCard } from "./components/Sortables/Sortables";

import { useState } from "react";

const data: SortableCard[] = [
  {
    id: 1,
    title: "One (Disabled)",
    description: "You can't move me. You can't reorder me.",
    isDraggingDisabled: true,
  },

  {
    id: 2,
    title: "Two",
    description: "You can move me.",
  },
  {
    id: 3,
    title: "Three (Disabled)",
    description: "You can't move me. You can't reorder me.",
    isDraggingDisabled: true,
  },
  {
    id: 4,
    title: "Four",
    description: "You can move me.",
  },
  {
    id: 5,
    title: "Five",
    description: "You can move me.",
  },
];

function App() {
  const [list, setList] = useState(data);

  //Remove item from the list through the onClose method
  const onCloseHandler = (id: number) => {
    const newList = [...list].filter((item) => item.id !== id);
    setList(newList);
  };

  //Add onClose method and disable cards
  list.forEach((item) => {
    if (item.id === 1 || item.id === 3) item.isDraggingDisabled = true;
    else item.onClose = (id) => onCloseHandler(id);
  });

  return (
    <div className="App">
      <Sortables data={list} />
    </div>
  );
}

export default App;
